function obtenerCateListarAdmin() {
    const apiObtenerCate = "http://localhost:8094/tiendavirtual/categorias/todas";
    const miPromesaCate = fetch(apiObtenerCate).then(respuesta => respuesta.json())
        .catch(miError => console.log(miError))

    Promise.all([miPromesaCate]).then((arregloDatos) => {
        const datos = arregloDatos[0];
        crearFilaCateListarAdmin(datos);
    });
}

function crearFilaCateListarAdmin(arrObjeto) {
    const cantidadRegistros = arrObjeto.length;
    for (let i = 0; i < cantidadRegistros; i++) {
        const codigo = arrObjeto[i].codCategoria;
        const nombre = arrObjeto[i].nombreCategoria;
        const estado = arrObjeto[i].estadoCategoria;
        let estadoTxt = "Activo";
        if(estado != 1){
            estadoTxt = "<span class='check-rojo'>Inactivo</span>";
        }

        document.getElementById("tablaCateAdmin").insertRow(-1).innerHTML = "<td class='text-center'>" + codigo + "</td>"
            + "<td class='text-center'>" + nombre + "</td>"
            + "<td class='text-center'>" + estadoTxt + "</td>"
            + "<td class='text-center'>"
            + " <a href='javascript:confirmarBorrarCategoria("
            + codigo+");'><i class='fa-solid fa-trash-can check-rojo'></i></a>&nbsp;"
            + "<a href='#cateupdate/"+ codigo + "'> <i class='fa-solid fa-pen-to-square'></i></a>"
            + "</td>";

    }
}

function confirmarBorrarCategoria (codigo){
    if(window.confirm("¿Seguro que quieres eliminar la categoria?")){
        window.location.replace("#catedelete/" + codigo);

    }
}