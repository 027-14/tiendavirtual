function actualizarCate() {
  const codigo = document.getElementById("cod").value;
  const nombre = document.getElementById("nom").value;
  const estado = document.getElementById("est").value;

  let objetoEnviar = {
    codCategoria: codigo,
    nombreCategoria: nombre,
    estadoCategoria: estado,
  };
  const api = "http://localhost:8094/tiendavirtual/categorias/editar";
  fetch(api, {
    method: "PUT",
    body: JSON.stringify(objetoEnviar),
    headers: { "Content-type": "application/json; charset=UTF-8" },
  })
    .then((respuesta) => respuesta.json())
    .then((datos) => {
      if (datos.hasOwnProperty("codCategoria")) {
        console.log(datos);
        document.getElementById("cateMsgOk").style.display = "";
        document.getElementById("cateMsgError").style.display = "none";
      } else {
        console.log("No se puede grabar -> " + datos.status);
        document.getElementById("cateMsgOk").style.display = "none";
        document.getElementById("cateMsgError").style.display = "";
      }
      window.location.replace("#cateadmin");
    })
    .catch((miError) => console.log(miError));
}