function obtenerProductoListar() {
    const apiObtenerPro = "http://localhost:8094/tiendavirtual/productos/activas";
    const miPromesaPro = fetch(apiObtenerPro).then(respuesta => respuesta.json())
        .catch(miError => console.log(miError))

    Promise.all([miPromesaPro]).then((arregloDatos) => {
        const datos = arregloDatos[0];
        crearFilaProListar(datos);
    });
}

function crearFilaProListar(arrObjeto) {
    const cantidadRegistros = arrObjeto.length;
    for (let i = 0; i < cantidadRegistros; i++) {
        const codigo = arrObjeto[i].codProducto;
        const nombre = arrObjeto[i].nombreProducto;
        const categoria = arrObjeto[i].codCategoria.nombreCategoria;
        const linea = arrObjeto[i].codLinea.nombreLinea;
        const proveedor = arrObjeto[i].codProveedor.nombreProveedor;
        const cantidad = arrObjeto[i].cantidadProducto;
        const precio = arrObjeto[i].precioProducto;
        const imagen = arrObjeto[i].imagen;

        document.getElementById("tablaProductoListar").insertRow(-1).innerHTML = "<td class='text-center align-middle'>" + codigo + "</td>"
            + "<td class='text-center align-middle'>" + nombre + "</td>"
            + "<td class='text-center align-middle'>" + categoria + "</td>"
            + "<td class='text-center align-middle'>" + linea + "</td>"
            + "<td class='text-center align-middle'>" + proveedor + "</td>"
            + "<td class='text-center align-middle'>" + cantidad + "</td>"
            + "<td class='text-center align-middle'>" + precio + "00</td>"
            + "<td><img class='miniaturaImagen' src='" + imagen + "' alt='no foto'/></td>";
    }
}