function mostrarProducto(codigo) {
  const apiObtenerPro =
    "http://localhost:8094/tiendavirtual/productos/uno/" + codigo;

  const miPromesaPro = fetch(apiObtenerPro)
    .then((respuesta) => respuesta.json())
    .then((dato) => {
      if (dato.hasOwnProperty("codProducto")) {
        document.getElementById("cod").value = dato.codProducto;
        document.getElementById("nom").value = dato.nombreProducto;
        document.getElementById("cat").value = dato.codCategoria.nombreCategoria;
        document.getElementById("lin").value = dato.codLinea.nombreLinea;
        document.getElementById("pro").value = dato.codProveedor.nombreProveedor;
        document.getElementById("est").value = dato.estadoProducto;
        document.getElementById("can").value = dato.cantidadProducto;
        document.getElementById("pre").value = dato.precioProducto;
      } else {
      }
    })
    .catch((miError) => console.log(miError));
}