function actualizarProducto() {
  const codigo = document.getElementById("cod").value;
  const nombre = document.getElementById("nom").value;
  const categoria = document.getElementById("cat").value;
  const linea = document.getElementById("lin").value;
  const proveedor = document.getElementById("prov").value;
  const estado = document.getElementById("est").value;
  const cantidad = document.getElementById("can").value;
  const precio = document.getElementById("pre").value;

  let objetoEnviar = {
    codProducto: codigo,
    nombreProducto: nombre,
    nomCategoria: categoria,
    nomLinea: linea,
    nomProveedor: proveedor,
    estadoProducto: estado,
    cantidad: cantidad,
    precio: precio,
  };
  const api = "http://localhost:8094/tiendavirtual/productos/editar";
  fetch(api, {
    method: "PUT",
    body: JSON.stringify(objetoEnviar),
    headers: { "Content-type": "application/json; charset=UTF-8" },
  })
    .then((respuesta) => respuesta.json())
    .then((datos) => {
      if (datos.hasOwnProperty("codProducto")) {
        console.log(datos);
        document.getElementById("proMsgOk").style.display = "";
        document.getElementById("proMsgError").style.display = "none";
      } else {
        console.log("No se puede grabar -> " + datos.status);
        document.getElementById("proMsgOk").style.display = "none";
        document.getElementById("proMsgError").style.display = "";
      }
      window.location.replace("#proadmin");
    })
    .catch((miError) => console.log(miError));
}