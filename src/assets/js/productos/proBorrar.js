function eliminarProducto(parametro) {
    const apiObtenerProve = "http://localhost:8094/tiendavirtual/productos/borrar/" + parametro;
    const miPromesaProve = fetch(apiObtenerProve,{method:"DELETE"}).then(respuesta => respuesta.json())
        .catch(miError => console.log(miError))

    Promise.all([miPromesaProve]).then((arregloDatos) => {
        const datos = arregloDatos[0];
        if (datos.status == "200") {
            document.getElementById("alertProductoEliminar").classList.add("alert-primary")
            document.getElementById("msgProductoEliminar").innerHTML="Producto eliminado correctamente"
        } else {
            document.getElementById("alertProductoEliminar").classList.add("alert-danger")
            document.getElementById("msgProductoEliminar").innerHTML="Producto no eliminado"
        }
    });
}