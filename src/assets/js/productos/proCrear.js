function crearProductos() {
    const nombre = document.getElementById('nom').value;

    let objetoEnviar = {
        nombreProducto: nombre,
        cantidadProducto: cantidad,
        precioProducto: precio,
        codCategoria: categoria,
        codLinea: linea,
        codProveedor: proveedor
    }
    const apiCrear = "http://localhost:8094/tiendavirtual/productos/crear";
    fetch(apiCrear, {
        method: "POST",
        body: JSON.stringify(objetoEnviar),
        headers: { "Content-type": "application/json; charset=UTF-8" }
    })
        .then(respuesta => respuesta.json())
        .then(datos => {
            if (datos.hasOwnProperty('codProducto')) {
                console.log(datos)
                document.getElementById("proMsgOk").style.display = "";
                document.getElementById("proMsgError").style.display = "none";
            } else {
                console.log("No se puede grabar -> " + datos.status);
                document.getElementById("proMsgOk").style.display = "none";
                document.getElementById("proMsgError").style.display = "";
            }

        })
        .catch(miError => console.log(miError));

    document.getElementById("formaProductoCrear").reset();
    document.getElementById("formaProductoCrear").classList.remove("was-validated");

}