function obtenerProductoListarAdmin() {
    const apiObtenerPro = "http://localhost:8094/tiendavirtual/productos/todos";
    const miPromesaPro = fetch(apiObtenerPro).then(respuesta => respuesta.json())
        .catch(miError => console.log(miError))

    Promise.all([miPromesaPro]).then((arregloDatos) => {
        const datos = arregloDatos[0];
        crearFilaProListarAdmin(datos);
    });
}

function crearFilaProListarAdmin(arrObjeto) {
    const cantidadRegistros = arrObjeto.length;
    for (let i = 0; i < cantidadRegistros; i++) {
        const codigo = arrObjeto[i].codProducto;
        const nombre = arrObjeto[i].nombreProducto;
        const categoria = arrObjeto[i].codCategoria.nombreCategoria;
        const linea = arrObjeto[i].codLinea.nombreLinea;
        const proveedor = arrObjeto[i].codProveedor.nombreProveedor;
        const cantidad = arrObjeto[i].cantidadProducto;
        const estado = arrObjeto[i].estadoProducto;
        const precio = arrObjeto[i].precioProducto;
        const imagen = arrObjeto[i].imagen;
        let estadoTxt = "Activo";
        if(estado != 1){
            estadoTxt = "<span class='check-rojo'>Inactivo</span>";
        }

        document.getElementById("tablaProductosAdmin").insertRow(-1).innerHTML = "<td class='text-center align-middle'>" + codigo + "</td>"
            + "<td class='text-center align-middle'>" + nombre + "</td>"
            + "<td class='text-center align-middle'>" + categoria + "</td>"
            + "<td class='text-center align-middle'>" + linea + "</td>"
            + "<td class='text-center align-middle'>" + proveedor + "</td>"
            + "<td class='text-center align-middle'>" + cantidad + "</td>"
            + "<td class='text-center align-middle'>" + estadoTxt + "</td>"
            + "<td class='text-center align-middle'>" + precio + "00</td>"
            + "<td><img class='miniaturaImagen' src='" + imagen + "' alt='no foto'/></td>"
            + "<td class='text-center align-middle'>"
            + " <a href='javascript:confirmarBorrarProducto("
            + codigo +");'><i class='fa-solid fa-trash-can check-rojo'></i></a>&nbsp;"
            + "<a href='#proupdate/"+ codigo + "'> <i class='fa-solid fa-pen-to-square'></i></a>"
            + "</td>";

    }
}

function confirmarBorrarProducto (codigo){
    if(window.confirm("¿Seguro que quieres eliminar el producto?")){
        window.location.replace("#supdelete/" + codigo);

    }
}