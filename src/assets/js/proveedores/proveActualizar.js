function actualizarProveedor() {
  const codigo = document.getElementById("cod").value;
  const nombre = document.getElementById("nom").value;
  const estado = document.getElementById("est").value;

  let objetoEnviar = {
    codProveedor: codigo,
    nombreProveedor: nombre,
    estadoProveedor: estado,
  };
  const api = "http://localhost:8094/tiendavirtual/proveedores/editar";
  fetch(api, {
    method: "PUT",
    body: JSON.stringify(objetoEnviar),
    headers: { "Content-type": "application/json; charset=UTF-8" },
  })
    .then((respuesta) => respuesta.json())
    .then((datos) => {
      if (datos.hasOwnProperty("codProveedor")) {
        console.log(datos);
        document.getElementById("proveMsgOk").style.display = "";
        document.getElementById("proveMsgError").style.display = "none";
      } else {
        console.log("No se puede grabar -> " + datos.status);
        document.getElementById("proveMsgOk").style.display = "none";
        document.getElementById("proveMsgError").style.display = "";
      }
      window.location.replace("#supadmin");
    })
    .catch((miError) => console.log(miError));
}