function crearProveedor() {
    const nombre = document.getElementById('nom').value;

    let objetoEnviar = {
        nombreProveedor: nombre,
    }
    const apiCrear = "http://localhost:8094/tiendavirtual/proveedores/crear";
    fetch(apiCrear, {
        method: "POST",
        body: JSON.stringify(objetoEnviar),
        headers: { "Content-type": "application/json; charset=UTF-8" }
    })
        .then(respuesta => respuesta.json())
        .then(datos => {
            if (datos.hasOwnProperty('codProveedor')) {
                console.log(datos)
                document.getElementById("proveMsgOk").style.display = "";
                document.getElementById("proveMsgError").style.display = "none";
            } else {
                console.log("No se puede grabar -> " + datos.status);
                document.getElementById("proveMsgOk").style.display = "none";
                document.getElementById("proveMsgError").style.display = "";
            }

        })
        .catch(miError => console.log(miError));

    document.getElementById("formaProveedorCrear").reset();
    document.getElementById("formaProveedorCrear").classList.remove("was-validated");
}