function obtenerProveedorListarAdmin() {
    const apiObtenerProve = "http://localhost:8094/tiendavirtual/proveedores/todos";
    const miPromesaProve = fetch(apiObtenerProve).then(respuesta => respuesta.json())
        .catch(miError => console.log(miError))

    Promise.all([miPromesaProve]).then((arregloDatos) => {
        const datos = arregloDatos[0];
        crearFilaProveListarAdmin(datos);
    });
}

function crearFilaProveListarAdmin(arrObjeto) {
    const cantidadRegistros = arrObjeto.length;
    for (let i = 0; i < cantidadRegistros; i++) {
        const codigo = arrObjeto[i].codProveedor;
        const nombre = arrObjeto[i].nombreProveedor;
        const estado = arrObjeto[i].estadoProveedor;
        let estadoTxt = "Activo";
        if(estado != 1){
            estadoTxt = "<span class='check-rojo'>Inactivo</span>";
        }

        document.getElementById("tablaProveAdmin").insertRow(-1).innerHTML = "<td class='text-center'>" + codigo + "</td>"
            + "<td class='text-center'>" + nombre + "</td>"
            + "<td class='text-center'>" + estadoTxt + "</td>"
            + "<td class='text-center'>"
            + " <a href='javascript:confirmarBorrarProveedor("
            + codigo+");'><i class='fa-solid fa-trash-can check-rojo'></i></a>&nbsp;"
            + "<a href='#supupdate/"+ codigo + "'> <i class='fa-solid fa-pen-to-square'></i></a>"
            + "</td>";

    }
}

function confirmarBorrarProveedor (codigo){
    if(window.confirm("¿Seguro que quieres eliminar el proveedor?")){
        window.location.replace("#supdelete/" + codigo);

    }
}