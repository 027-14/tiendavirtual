function mostrarProveedor(codigo) {
  const apiObtenerProve =
    "http://localhost:8094/tiendavirtual/proveedores/uno/" + codigo;

  const miPromesaProve = fetch(apiObtenerProve)
    .then((respuesta) => respuesta.json())
    .then((dato) => {
      if (dato.hasOwnProperty("codProveedor")) {
        document.getElementById("cod").value = dato.codProveedor;
        document.getElementById("nom").value = dato.nombreProveedor;
        document.getElementById("est").value = dato.estadoProveedor;
      } else {
      }
    })
    .catch((miError) => console.log(miError));
}