function obtenerProveedorListar() {
    const apiObtenerProve = "http://localhost:8094/tiendavirtual/proveedores/activos";
    const miPromesaProve = fetch(apiObtenerProve).then(respuesta => respuesta.json())
        .catch(miError => console.log(miError))

    Promise.all([miPromesaProve]).then((arregloDatos) => {
        const datos = arregloDatos[0];
        crearFilaProveListar(datos);
    });
}

function crearFilaProveListar(arrObjeto) {
    const cantidadRegistros = arrObjeto.length;
    for (let i = 0; i < cantidadRegistros; i++) {
        const codigo = arrObjeto[i].codProveedor;
        const nombre = arrObjeto[i].nombreProveedor;

        document.getElementById("tablaProveedorListar").insertRow(-1).innerHTML = "<td class='text-center'>" + codigo + "</td>"
            + "<td class='text-center'>" + nombre + "</td>";
    }
}