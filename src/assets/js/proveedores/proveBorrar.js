function eliminarProveedor(parametro) {
    const apiObtenerProve = "http://localhost:8094/tiendavirtual/proveedores/borrar/" + parametro;
    const miPromesaProve = fetch(apiObtenerProve, { method: "DELETE" }).then(respuesta => respuesta.json())
        .catch(miError => console.log(miError))

    Promise.all([miPromesaProve]).then((arregloDatos) => {
        const datos = arregloDatos[0];
        if (datos.status == "200") {
            document.getElementById("alertProveedorEliminar").classList.add("alert-primary")
            document.getElementById("msgProveedorEliminar").innerHTML = "Proveedor eliminado correctamente"
        } else {
            document.getElementById("alertProveedorEliminar").classList.add("alert-danger")
            document.getElementById("msgProveedorEliminar").innerHTML = "Proveedor no eliminado"
        }
    });
}