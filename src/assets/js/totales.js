function obtenerCantidad() {

    const apiObtenerProve = "http://localhost:8094/tiendavirtual/proveedores/cantidad";
    const miPromesaProve = fetch(apiObtenerProve)
        .then((respuesta) => respuesta.json())
        .then((dato) => {
            document.getElementById("proveedor").innerHTML = "<i class='fa-solid fa-handshake fa-6x'></i><br><br>" + dato + " proveedores registrados";
        })
        .catch((miError) => console.log(miError));


    const apiObtenerCate = "http://localhost:8094/tiendavirtual/categorias/cantidad";
    const miPromesaCate = fetch(apiObtenerCate)
        .then((respuesta) => respuesta.json())
        .then((dato) => {
            document.getElementById("categorias").innerHTML = "<i class='fa-solid fa-tags fa-6x'></i><br><br>" + dato + " categorías registradas";
        })
        .catch((miError) => console.log(miError));

    const apiObtenerLinea = "http://localhost:8094/tiendavirtual/lineas/cantidad";
    const miPromesaLinea = fetch(apiObtenerLinea)
        .then((respuesta) => respuesta.json())
        .then((dato) => {
            document.getElementById("lineas").innerHTML = "<i class='fa-solid fa-boxes-packing fa-6x'></i><br><br>" + dato + " lineas registradas";
        })
        .catch((miError) => console.log(miError));

    const apiObtenerPro = "http://localhost:8094/tiendavirtual/productos/cantidad";
    const miPromesaPro = fetch(apiObtenerPro)
        .then((respuesta) => respuesta.json())
        .then((dato) => {
            document.getElementById("productos").innerHTML = "<i class='fa-solid fa-gifts fa-6x'></i><br><br>" + dato + " productos registrados";
        })
        .catch((miError) => console.log(miError));

    const apiObtenerProd = "http://localhost:8094/tiendavirtual/productos/activas";
    const miPromesaProd = fetch(apiObtenerProd)
        .then((respuesta) => respuesta.json())
        .then((dato) => {
            document.getElementById("nuevosproductos").innerHTML = "<i class='fa-solid fa-boxes-stacked fa-6x'></i><br><br>" + dato.length + " productos disponibles";
        })
        .catch((miError) => console.log(miError));

}