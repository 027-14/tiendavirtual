function logicaNegocio(url, param) {
  switch (url) {
    // Categorias
    // ***************************************************************
    case "src/componentes/categorias/catelistar.html":
      obtenerCateListar();
      break;

    case "src/componentes/categorias/cateadmin.html":
      obtenerCateListarAdmin();
      break;

    case "src/componentes/categorias/cateborrar.html":
      eliminarCate(param);
      break;

    case "src/componentes/categorias/cateactualizar.html":
      mostrarCategoria(param);
      break;

    // Lineas
    // ***************************************************************
    case "src/componentes/lineas/linelistar.html":
      obtenerLineaListar();
      break;

    case "src/componentes/lineas/lineadmin.html":
      obtenerLineaListarAdmin();
      break;

    case "src/componentes/lineas/lineborrar.html":
      eliminarLinea(param);
      break;

    case "src/componentes/lineas/lineactualizar.html":
      mostrarLinea(param);
      break;

    // Proveedores
    // ***************************************************************
    case "src/componentes/proveedores/provelistar.html":
      obtenerProveedorListar();
      break;

    case "src/componentes/proveedores/proveadmin.html":
      obtenerProveedorListarAdmin();
      break;

    case "src/componentes/proveedores/proveborrar.html":
      eliminarProveedor(param);
      break;

    case "src/componentes/proveedores/proveactualizar.html":
      mostrarProveedor(param);
      break;

    // Producto
    // ***************************************************************
    case "src/componentes/productos/prolistar.html":
      obtenerProductoListar();
      break;

    case "src/componentes/productos/proadmin.html":
      obtenerProductoListarAdmin();
      break;

    case "src/componentes/productos/proborrar.html":
      eliminarProducto(param);
      break;

    case "src/componentes/productos/proactualizar.html":
      mostrarProducto(param);
      break;

    // Inicio
    // ***************************************************************
    case "src/componentes/inicio.html":
      obtenerCantidad();
      break;

    default:
      console.log("Componente no requiere Javascript personalizado");
  }
}
