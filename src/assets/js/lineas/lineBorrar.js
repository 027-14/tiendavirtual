function eliminarLinea(parametro) {
    const apiObtenerLine = "http://localhost:8094/tiendavirtual/lineas/borrar/" + parametro;
    const miPromesaLine = fetch(apiObtenerLine,{method:"DELETE"}).then(respuesta => respuesta.json())
        .catch(miError => console.log(miError))

    Promise.all([miPromesaLine]).then((arregloDatos) => {
        const datos = arregloDatos[0];
        if (datos.status == "200") {
            document.getElementById("alertLineEliminar").classList.add("alert-primary")
            document.getElementById("msgLineEliminar").innerHTML="Linea eliminada correctamente"
        } else {
            document.getElementById("alertLineEliminar").classList.add("alert-danger")
            document.getElementById("msgLineEliminar").innerHTML="Linea no eliminada"
        }
    });
}