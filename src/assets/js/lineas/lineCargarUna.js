function mostrarLinea(codigo) {
  const apiObtenerLine =
    "http://localhost:8094/tiendavirtual/lineas/uno/" + codigo;

  const miPromesaLine = fetch(apiObtenerLine)
    .then((respuesta) => respuesta.json())
    .then((dato) => {
      if (dato.hasOwnProperty("codLinea")) {
        document.getElementById("cod").value = dato.codLinea;
        document.getElementById("nom").value = dato.nombreLinea;
        document.getElementById("est").value = dato.estadoLinea;
      } else {
      }
    })
    .catch((miError) => console.log(miError));
}
