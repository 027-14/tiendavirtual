function obtenerLineaListar() {
    const apiObtenerLine = "http://localhost:8094/tiendavirtual/lineas/activas";
    const miPromesaLine = fetch(apiObtenerLine).then(respuesta => respuesta.json())
        .catch(miError => console.log(miError))

    Promise.all([miPromesaLine]).then((arregloDatos) => {
        const datos = arregloDatos[0];
        crearFilaLineListar(datos);
    });
}

function crearFilaLineListar(arrObjeto) {
    const cantidadRegistros = arrObjeto.length;
    for (let i = 0; i < cantidadRegistros; i++) {
        const codigo = arrObjeto[i].codLinea;
        const nombre = arrObjeto[i].nombreLinea;

        document.getElementById("tablaLineListar").insertRow(-1).innerHTML = "<td class='text-center'>" + codigo + "</td>"
            + "<td class='text-center'>" + nombre + "</td>";
    }
}