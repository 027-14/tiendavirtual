function crearLinea() {
    const nombre = document.getElementById('nom').value;

    let objetoEnviar = {
        nombreLinea: nombre,
    }
    const apiCrear = "http://localhost:8094/tiendavirtual/lineas/crear";
    fetch(apiCrear, {
        method: "POST",
        body: JSON.stringify(objetoEnviar),
        headers: { "Content-type": "application/json; charset=UTF-8" }
    })
        .then(respuesta => respuesta.json())
        .then(datos => {
            if (datos.hasOwnProperty('codLinea')) {
                console.log(datos)
                document.getElementById("lineMsgOk").style.display = "";
                document.getElementById("lineMsgError").style.display = "none";
            } else {
                console.log("No se puede grabar -> " + datos.status);
                document.getElementById("lineMsgOk").style.display = "none";
                document.getElementById("lineMsgError").style.display = "";
            }

        })
        .catch(miError => console.log(miError));

    document.getElementById("formaLineCrear").reset();
    document.getElementById("formaLineCrear").classList.remove("was-validated");

}