function actualizarLineas() {
    const codigo = document.getElementById("cod").value;
    const nombre = document.getElementById("nom").value;
    const estado = document.getElementById("est").value;

    let objetoEnviar = {
      codLinea: codigo,
      nombreLinea: nombre,
      estadoLinea: estado,
    };
    const api = "http://localhost:8094/tiendavirtual/lineas/editar";
    fetch(api, {
      method: "PUT",
      body: JSON.stringify(objetoEnviar),
      headers: { "Content-type": "application/json; charset=UTF-8" },
    })
      .then((respuesta) => respuesta.json())
      .then((datos) => {
        if (datos.hasOwnProperty("codLinea")) {
          console.log(datos);
          document.getElementById("lineMsgOk").style.display = "";
          document.getElementById("lineMsgError").style.display = "none";
        } else {
          console.log("No se puede grabar -> " + datos.status);
          document.getElementById("lineMsgOk").style.display = "none";
          document.getElementById("lineMsgError").style.display = "";
        }
        window.location.replace("#lineadmin");
      })
      .catch((miError) => console.log(miError));
  }