function obtenerLineaListarAdmin() {
    const apiObtenerLine = "http://localhost:8094/tiendavirtual/lineas/todas";
    const miPromesaLine = fetch(apiObtenerLine).then(respuesta => respuesta.json())
        .catch(miError => console.log(miError))

    Promise.all([miPromesaLine]).then((arregloDatos) => {
        const datos = arregloDatos[0];
        crearFilaLineListarAdmin(datos);
    });
}

function crearFilaLineListarAdmin(arrObjeto) {
    const cantidadRegistros = arrObjeto.length;
    for (let i = 0; i < cantidadRegistros; i++) {
        const codigo = arrObjeto[i].codLinea;
        const nombre = arrObjeto[i].nombreLinea;
        const estado = arrObjeto[i].estadoLinea;
        let estadoTxt = "Activo";
        if(estado != 1){
            estadoTxt = "<span class='check-rojo'>Inactivo</span>";
        }

        document.getElementById("tablaLineAdmin").insertRow(-1).innerHTML = "<td class='text-center'>" + codigo + "</td>"
            + "<td class='text-center'>" + nombre + "</td>"
            + "<td class='text-center'>" + estadoTxt + "</td>"
            + "<td class='text-center'>"
            + " <a href='javascript:confirmarBorrarLinea("
            + codigo+");'><i class='fa-solid fa-trash-can check-rojo'></i></a>&nbsp;"
            + "<a href='#lineupdate/"+ codigo + "'> <i class='fa-solid fa-pen-to-square'></i></a>"
            + "</td>";

    }
}

function confirmarBorrarLinea (codigo){
    if(window.confirm("¿Seguro que quieres eliminar la linea?")){
        window.location.replace("#linedelete/" + codigo);

    }
}