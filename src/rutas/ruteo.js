'use strict';

(function () {
    function init() {
        var router = new Router([
            new Route('home', 'inicio.html', true),

            // Rutas para categorias
            new Route('catelist', 'categorias/catelistar.html'),
            new Route('cateadd', 'categorias/catecrear.html'),
            new Route('cateadmin', 'categorias/cateadmin.html'),
            new Route('catedelete', 'categorias/cateborrar.html'),
            new Route('cateupdate', 'categorias/cateactualizar.html'),

            // Rutas para lineas
            new Route('linelist', 'lineas/linelistar.html'),
            new Route('lineadd', 'lineas/linecrear.html'),
            new Route('lineadmin', 'lineas/lineadmin.html'),
            new Route('linedelete', 'lineas/lineborrar.html'),
            new Route('lineupdate', 'lineas/lineactualizar.html'),

            // Rutas para proveedores
            new Route('suplist', 'proveedores/provelistar.html'),
            new Route('supadd', 'proveedores/provecrear.html'),
            new Route('supadmin', 'proveedores/proveadmin.html'),
            new Route('supdelete', 'proveedores/proveborrar.html'),
            new Route('supupdate', 'proveedores/proveactualizar.html'),

            // Rutas para productos
            new Route('prolist', 'productos/prolistar.html'),
            new Route('proadd', 'productos/procrear.html'),
            new Route('proadmin', 'productos/proadmin.html'),
            new Route('prodelete', 'productos/proborrar.html'),
            new Route('proupdate', 'productos/proactualizar.html'),

            // Rutas para otros añadidos, etc...
            new Route('addclient', 'clienteagregar.html'),
            new Route('listclient', 'clientelistar.html'),
            new Route('searchclient', 'clientebuscar.html'),
            new Route('proadminis', 'admin.html'),
            new Route('login', 'acceso.html')
        ]);
    }
    init();
}());
